package com.blinkseven.banditboard.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import static com.blinkseven.banditboard.data.BanditContract.*;

public class BanditProvider extends ContentProvider {
    public static final String LOG_TAG = BanditProvider.class.getSimpleName();

    public static final int FAVORITES = 100;
    public static final int FAVORITE_ID = 101;

    public static final int PHRASES = 200;
    public static final int PHRASE_ID = 201;

    public static final int IDLE = 300;
    public static final int IDLE_ID = 301;

    public static final int JOKES = 400;
    public static final int JOKE_ID = 401;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        // Favorite
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_FAVORITE, FAVORITES);
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_FAVORITE + "/#", FAVORITE_ID);

        // Phrases
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_PHRASES, PHRASES);
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_PHRASES + "/#", PHRASE_ID);

        // Idle
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_IDLE, IDLE);
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_IDLE + "/#", IDLE_ID);

        // Jokes
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_JOKES, JOKES);
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_JOKES + "/#", JOKE_ID);
    }

    private BanditDbHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = new BanditDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        Cursor cursor;

        int match = sUriMatcher.match(uri);
        switch (match) {
            // Favorite's case
            case FAVORITES:
                cursor = database.query(
                        FavoriteEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case FAVORITE_ID:
                selection = FavoriteEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(
                        FavoriteEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            // Phrases' case
            case PHRASES:
                cursor = database.query(
                        PhraseEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case PHRASE_ID:
                selection = PhraseEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(
                        PhraseEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            // Idle's case
            case IDLE:
                cursor = database.query(
                        IdleEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case IDLE_ID:
                selection = IdleEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(
                        IdleEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            // Jokes' case
            case JOKES:
                cursor = database.query(
                        JokeEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case JOKE_ID:
                selection = JokeEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(
                        JokeEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case FAVORITES:
                return insertNewFavorite(uri, values);
            case FAVORITE_ID:
                return reorderFavorite(uri, values);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    /**
     * Insert an item into the database with the given content values. Return the new content URI
     * for that specific row in the database.
     */
    private Uri insertNewFavorite(Uri uri, ContentValues values) {
        String name = values.getAsString(FavoriteEntry.COLUMN_FAVORITE_NAME);
        if (name == null)
            throw new IllegalArgumentException("Favorite requires a name");

        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long id = database.insert(FavoriteEntry.TABLE_NAME, null, values);
        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row for " + uri);
            return null;
        }

        return ContentUris.withAppendedId(uri, id);
    }

    private Uri reorderFavorite(Uri uri, ContentValues values) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        String selection = FavoriteEntry._ID + " = ?";
        long oldId = ContentUris.parseId(uri);
        String[] selectionArgs = {String.valueOf(oldId)};
        database.delete(FavoriteEntry.TABLE_NAME, selection, selectionArgs);

        long newId = values.getAsLong(FavoriteEntry._ID);
        ContentValues updatedValues = new ContentValues();
        if (newId > oldId) {
            for (long l = oldId + 1; l <= newId; l++) {
                updatedValues.clear();
                updatedValues.put(FavoriteEntry._ID, l - 1);
                selectionArgs = new String[]{String.valueOf(l)};
                database.update(FavoriteEntry.TABLE_NAME, updatedValues, selection, selectionArgs);
                updatedValues.clear();
            }
        } else {
            for (long l = oldId - 1; l >= newId; l--) {
                updatedValues.clear();
                updatedValues.put(FavoriteEntry._ID, l + 1);
                selectionArgs = new String[]{String.valueOf(l)};
                database.update(FavoriteEntry.TABLE_NAME, updatedValues, selection, selectionArgs);
                updatedValues.clear();
            }
        }

        long resultId = database.insert(FavoriteEntry.TABLE_NAME, null, values);

        if (resultId == newId) {
            return ContentUris.withAppendedId(FavoriteEntry.CONTENT_URI, resultId);
        } else {
            try {
                throw new Exception("Reorder failed");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return uri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (values.size() == 0)
            return 0;

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int rowsUpdated;

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case PHRASES:
                rowsUpdated = database.update(PhraseEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case IDLE:
                rowsUpdated = database.update(IdleEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case JOKES:
                rowsUpdated = database.update(JokeEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }

        if (rowsUpdated != 0)
            getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        int rowsDeleted;

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case FAVORITES:
                rowsDeleted = database.delete(FavoriteEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }

        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDeleted;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case FAVORITES:
                return FavoriteEntry.CONTENT_LIST_TYPE;
            case FAVORITE_ID:
                return FavoriteEntry.CONTENT_ITEM_TYPE;
            case PHRASES:
                return PhraseEntry.CONTENT_LIST_TYPE;
            case PHRASE_ID:
                return PhraseEntry.CONTENT_ITEM_TYPE;
            case IDLE:
                return IdleEntry.CONTENT_LIST_TYPE;
            case IDLE_ID:
                return IdleEntry.CONTENT_ITEM_TYPE;
            case JOKES:
                return JokeEntry.CONTENT_LIST_TYPE;
            case JOKE_ID:
                return JokeEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }
}
