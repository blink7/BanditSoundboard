package com.blinkseven.banditboard.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class BanditContract {
    public static final String CONTENT_AUTHORITY = "com.blinkseven.banditboard";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_FAVORITE = "favorite";
    public static final String PATH_PHRASES = "phrases";
    public static final String PATH_IDLE = "idle";
    public static final String PATH_JOKES = "jokes";

    private BanditContract() {
    }

    public static final class FavoriteEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_FAVORITE);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FAVORITE;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FAVORITE;

        public final static String TABLE_NAME = "favorite";

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_FAVORITE_NAME ="name";
        public final static String COLUMN_FAVORITE_TABLE ="table_src";
    }

    public static class BaseEntry implements BaseColumns {
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_NAME ="name";
        public final static String COLUMN_FAVORITE ="favorite";
        public final static String COLUMN_SUBTITLE ="subtitle";
        public final static String COLUMN_SUBTITLE_POSITION ="subtitle_position";

        public static final int NOT_FAVORITE = 0;
        public static final int FAVORITE = 1;
    }

    public static final class PhraseEntry extends BaseEntry {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PHRASES);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PHRASES;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PHRASES;

        public final static String TABLE_NAME = "phrases";
    }

    public static final class IdleEntry extends BaseEntry {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_IDLE);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_IDLE;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_IDLE;

        public final static String TABLE_NAME = "idle";
    }

    public static final class JokeEntry extends BaseEntry {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_JOKES);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_JOKES;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_JOKES;

        public final static String TABLE_NAME = "jokes";
    }
}
