package com.blinkseven.banditboard.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BanditDbHelper extends SQLiteOpenHelper {
    public static final String LOG_TAG = BanditDbHelper.class.getSimpleName();

    private Context mContext;

    private static final String DATABASE_NAME = "bandit.db";
    private static final int DATABASE_VERSION = 1;

    SQLiteDatabase mDatabase;

    public BanditDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context;
        mDatabase = openDatabase();
    }

    public SQLiteDatabase openDatabase() {
        File dbFile = mContext.getDatabasePath(DATABASE_NAME);

        if (!dbFile.exists()) {
            try {
                copyDatabase(dbFile);
            } catch (IOException e) {
                throw new RuntimeException("Error creating source database", e);
            }
        }

        return SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
    }

    private void copyDatabase(File dbFile) throws IOException {
        InputStream in = mContext.getAssets().open(DATABASE_NAME);
        dbFile.getParentFile().mkdirs();
        Log.d(LOG_TAG, dbFile.getParentFile().getAbsolutePath());
        OutputStream out = new FileOutputStream(dbFile);

        byte[] buffer = new byte[1024];
        while (in.read(buffer) > 0) {
            out.write(buffer);
        }

        out.flush();
        out.close();
        in.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    @Override
    public synchronized void close() {
        if (mDatabase != null) {
            mDatabase.close();
        }

        super.close();
    }
}
