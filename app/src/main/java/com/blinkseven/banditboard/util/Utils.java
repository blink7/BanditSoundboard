package com.blinkseven.banditboard.util;

import android.content.Context;
import android.content.res.Resources;

public class Utils {

    private Utils() {}

    public static float pxToDp(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static String getString(Context context, String name) {
        Resources resources = context.getResources();
        String packageName = context.getPackageName();
        String title;
        try {
            title = context.getString(
                    resources.getIdentifier(name, "string", packageName));
        } catch (Exception e) {
            title = name;
        }
        return title;
    }
}
