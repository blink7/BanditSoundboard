package com.blinkseven.banditboard.model.item;

public interface Item {
    String getName();

    boolean isPlaying();

    void setPlaying(boolean playing);
}
