package com.blinkseven.banditboard.model.item;

public class FavoriteItem implements Item {
    private String name;
    private String table;
    private boolean playing = NOT_PLAYING;

    private static final boolean NOT_PLAYING = false;

    public FavoriteItem(String name, String table) {
        this.name = name;
        this.table = table;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getTable() {
        return table;
    }

    @Override
    public boolean isPlaying() {
        return playing;
    }

    @Override
    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FavoriteItem that = (FavoriteItem) o;

        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
