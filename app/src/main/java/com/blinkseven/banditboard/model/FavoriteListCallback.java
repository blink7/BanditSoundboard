package com.blinkseven.banditboard.model;

public interface FavoriteListCallback {
    void onItemAdded(String tableName, String itemName);

    void onItemRemoved(String itemName);

    void onClearList();

    boolean isEmpty();

    void onReleasePlayer();
}
