package com.blinkseven.banditboard.model;

import android.content.Context;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;

import com.blinkseven.banditboard.model.item.Item;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class SoundPlayer {
    public static final String LOG_TAG = SoundPlayer.class.getSimpleName();

    private Context mContext;
    private RecyclerView.Adapter mAdapter;
    private List<? extends Item> mItems;

    private MediaPlayer mMediaPlayer;
    private AudioManager mAudioManager;

    private LinkedList<Integer> mPositions = new LinkedList<>();

    private AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {
                    if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                        //TODO: what to do if audio focus gain
                    } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                        releaseMediaPlayer();
                    } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                            focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                        releaseMediaPlayer();
                    }
                }
            };

    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            releaseMediaPlayer();
        }
    };

    public SoundPlayer(Context c, RecyclerView.Adapter adapter, List<? extends Item> items) {
        mContext = c;
        mAdapter = adapter;
        mItems = items;
    }

    public void onPlayClick(int position) {
        if (!mPositions.contains(position)) {
            mPositions.add(position);
        }

        if (mItems.get(position).isPlaying() && mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying())
                mMediaPlayer.pause();
            else
                mMediaPlayer.start();
        } else {
            releaseMediaPlayer();
            playSound();

            setAnimationIcon(true);
        }
    }

    private void playSound() {
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        int result = mAudioManager.requestAudioFocus(
                mOnAudioFocusChangeListener,
                AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            Resources resources = mContext.getResources();
            String packageName = mContext.getPackageName();

            int soundResId;

            String soundName = mItems.get(mPositions.peekLast()).getName();
            if (soundName.startsWith("random_")) {
                int randomIndex = new Random().nextInt(mItems.size() - 1) + 1;
                soundResId = resources.getIdentifier(
                        mItems.get(randomIndex).getName(),
                        "raw",
                        packageName);
            } else {
                soundResId = resources.getIdentifier(
                        soundName,
                        "raw",
                        packageName);
            }

            mMediaPlayer = MediaPlayer.create(mContext, soundResId);
            mMediaPlayer.start();
            mMediaPlayer.setOnCompletionListener(mCompletionListener);
        }
    }

    private void setAnimationIcon(boolean state) {
        int index;
        if (!state) {
            index = mPositions.poll();
            mItems.get(index).setPlaying(false);
            mAdapter.notifyItemChanged(index);
        } else {
            index = mPositions.peekLast();
            mItems.get(index).setPlaying(true);
            mAdapter.notifyItemChanged(index);
        }
    }

    public void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;

            mAudioManager.abandonAudioFocus(mOnAudioFocusChangeListener);

            setAnimationIcon(false);
        }
    }
}
