package com.blinkseven.banditboard.model.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.blinkseven.banditboard.R;
import com.blinkseven.banditboard.data.BanditContract;
import com.blinkseven.banditboard.ui.FavoriteFragment;
import com.blinkseven.banditboard.ui.ListFragment;

public class PagerAdapter extends FragmentPagerAdapter {
    private Context mContext;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FavoriteFragment();
            case 1:
                return ListFragment.newInstance(
                        BanditContract.PhraseEntry.TABLE_NAME,
                        BanditContract.PhraseEntry.CONTENT_URI);
            case 2:
                return ListFragment.newInstance(
                        BanditContract.IdleEntry.TABLE_NAME,
                        BanditContract.IdleEntry.CONTENT_URI);
            case 3:
                return ListFragment.newInstance(
                        BanditContract.JokeEntry.TABLE_NAME,
                        BanditContract.JokeEntry.CONTENT_URI);
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.favorite);
            case 1:
                return mContext.getString(R.string.phrases);
            case 2:
                return mContext.getString(R.string.idle);
            case 3:
                return mContext.getString(R.string.jokes);
            default:
                return null;
        }
    }
}
