package com.blinkseven.banditboard.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.blinkseven.banditboard.R;
import com.google.firebase.crash.FirebaseCrash;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.os.Environment.DIRECTORY_MUSIC;

public class FilesResolver {
    private static final String LOG_TAG = FilesResolver.class.getSimpleName();

    private Context mContext;

    public FilesResolver(Context context) {
        mContext = context;
    }

    public void share(String name) {
        createExternalStoragePrivateFile(name);
    }

    public void download(String name) {
        createExternalStoragePublicFile(name);
    }

    private void createExternalStoragePrivateFile(String name) {
        final Resources resources = mContext.getResources();
        String packageName = mContext.getPackageName();
        int stringResId = resources.getIdentifier(name, "string", packageName);
        String fileName = mContext.getString(stringResId) + ".ogg";
        File path = mContext.getExternalFilesDir(DIRECTORY_MUSIC);
        File file = new File(path, fileName);

        try {
            // Very simple code to copy a sound from the application's
            // resource into the external file. Note that if external storage is
            // not currently mounted this will silently fail.
            int soundResId = resources.getIdentifier(name, "raw", packageName);
            InputStream is = resources.openRawResource(soundResId);

            OutputStream os;
            if (isExternalStorageWritable()) {
                os = new FileOutputStream(file);
            } else {
                File downloadsDir = new File(mContext.getCacheDir().toString() + "/Music");
                downloadsDir.mkdirs();
                os = new FileOutputStream(new File(downloadsDir, fileName));
            }

            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            scanFile(file);

        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            FirebaseCrash.logcat(Log.ERROR, LOG_TAG, "Unable to create a private file");
            FirebaseCrash.report(e);
        }
    }

    private void scanFile(File file) {
        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(mContext,
                new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        createIntent(uri);
                    }
                });
    }

    private void createIntent(Uri uriToSound) {
        Intent intent = new Intent(Intent.ACTION_SEND)
                .setType("audio/ogg").putExtra(Intent.EXTRA_STREAM, uriToSound);
        mContext.startActivity(intent);
    }

    public void deleteExternalStoragePrivateFile(String name) {
        // Get path for the file on external storage.  If external
        // storage is not currently mounted this will fail.
        Resources resources = mContext.getResources();
        String packageName = mContext.getPackageName();
        int stringResId = resources.getIdentifier(name, "string", packageName);
        String fileName = mContext.getString(stringResId) + ".ogg";
        File file = new File(mContext.getExternalFilesDir(DIRECTORY_MUSIC), fileName);
        if (file != null) {
            file.deleteOnExit();
        }
    }

    private void createExternalStoragePublicFile(String name) {
        final Resources resources = mContext.getResources();
        String packageName = mContext.getPackageName();
        int stringResId = resources.getIdentifier(name, "string", packageName);
        final String fileName = mContext.getString(stringResId) + ".ogg";

        File path;
        if (isExternalStorageWritable()) {
            path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS);
        } else {
            path = new File(Environment.getExternalStorageDirectory().toString() + "/Download");
            path.mkdirs();
        }
        File file = new File(path, fileName);

        try {
            // Very simple code to copy a sound from the application's
            // resource into the external file. Note that if external storage is
            // not currently mounted this will silently fail.
            int soundResId = resources.getIdentifier(name, "raw", packageName);

            InputStream is = resources.openRawResource(soundResId);
            OutputStream os = new FileOutputStream(file);

            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            Toast.makeText(mContext, resources
                    .getString(R.string.file_downloaded), Toast.LENGTH_SHORT).show();

            MediaScannerConnection.scanFile(mContext,
                    new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            showDownloadFinishedNotification(uri, fileName);
                        }
                    });
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            Toast.makeText(mContext, resources
                    .getString(R.string.file_not_downloaded), Toast.LENGTH_SHORT).show();

            FirebaseCrash.logcat(Log.ERROR, LOG_TAG, "Unable to create file");
            FirebaseCrash.report(e);
        }
    }

    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private void showDownloadFinishedNotification(Uri uri, String fileName) {
        final int NOTIFICATION_ID = 12;
        int requestID = (int) System.currentTimeMillis();

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "audio/*");
        PendingIntent pendingIntent = PendingIntent.getActivity(
                mContext,
                requestID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notify = new NotificationCompat.Builder(mContext)
                .setContentTitle(fileName)
                .setContentText(mContext.getString(R.string.file_downloaded_dest))
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setContentIntent(pendingIntent)
                .build();

        notify.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager
                = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notify);
    }
}
