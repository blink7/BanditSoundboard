package com.blinkseven.banditboard.model.item;

public class ListItem implements Item {
    private String name;
    private boolean favorite;
    private String subtitle;
    private int subtitlePosition;
    private boolean playing = NOT_PLAYING;

    private static final boolean NOT_PLAYING = false;

    public ListItem(String name, boolean favorite, String subtitle, int subtitlePosition) {
        this.name = name;
        this.favorite = favorite;
        this.subtitle = subtitle;
        this.subtitlePosition = subtitlePosition;
    }

    @Override
    public String getName() {
        return name;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public int getSubtitlePosition() {
        return subtitlePosition;
    }

    @Override
    public boolean isPlaying() {
        return playing;
    }

    @Override
    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ListItem item = (ListItem) o;

        return name.equals(item.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
