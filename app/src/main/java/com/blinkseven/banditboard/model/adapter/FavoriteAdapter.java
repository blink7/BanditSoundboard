package com.blinkseven.banditboard.model.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinkseven.banditboard.R;
import com.blinkseven.banditboard.model.item.FavoriteItem;

import static com.blinkseven.banditboard.util.Utils.getString;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
    private List<FavoriteItem> mItems;

    private ItemClickCallback mItemClickCallback;

    public interface ItemClickCallback {
        void onPlayClick(int position);
        void onStartDrag(ViewHolder holder);
        void onContextMenuClick(int position);
    }

    public FavoriteAdapter(List<FavoriteItem> items, ItemClickCallback itemClickCallback) {
        mItems = items;
        mItemClickCallback = itemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fave_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Context context = holder.itemView.getContext();

        Animation radAnim = AnimationUtils.loadAnimation(context, R.anim.rad_circle_rotate);
        FavoriteItem item = mItems.get(position);

        String title = getString(context, item.getName());

        holder.mTitle.setText(title);
        holder.mReorder.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mItemClickCallback.onStartDrag(holder);
                return true;
            }
        });
        if (item.isPlaying()) {
            if (holder.mPlayingStatus.getAnimation() == null) {
                holder.mPlayingStatus.startAnimation(radAnim);
            }
            // Keeping the screen on while playing a sound
            holder.mContainer.setKeepScreenOn(true);
        } else {
            if (holder.mPlayingStatus.getAnimation() != null) {
                holder.mPlayingStatus.setAnimation(null);
            }
            // Release keeping the screen on
            holder.mContainer.setKeepScreenOn(false);
        }
        holder.mContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mItemClickCallback.onContextMenuClick(holder.getAdapterPosition());
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        if (holder.mPlayingStatus.getAnimation() != null)
            holder.mPlayingStatus.getAnimation().start();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView mTitle;
        private ImageView mPlayingStatus;
        private ImageView mReorder;
        private View mContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.lbl_item_text);
            mPlayingStatus = (ImageView) itemView.findViewById(R.id.stts_item_icon);
            mReorder = (ImageView) itemView.findViewById(R.id.reorder_item_icon);
            mContainer = itemView.findViewById(R.id.cont_item_root);
            mContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.cont_item_root) {
                mItemClickCallback.onPlayClick(getAdapterPosition());
            }
        }
    }
}
