package com.blinkseven.banditboard.model;

public interface ListCallback {
    void onItemRemoved (String tableName, String itemName);

    void onReleasePlayer();
}
