package com.blinkseven.banditboard.model.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinkseven.banditboard.R;
import com.blinkseven.banditboard.model.item.ListItem;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import static com.blinkseven.banditboard.util.Utils.getString;

import java.util.List;

public class ListAdapter
        extends RecyclerView.Adapter<ListAdapter.ViewHolder>
        implements StickyRecyclerHeadersAdapter<ListAdapter.HeaderViewHolder> {

    private List<ListItem> mItems;

    private ItemClickCallback mItemClickCallback;

    public interface ItemClickCallback {
        void onPlayClick(int position);
        void onFavoriteClick(int position);
        void onContextMenuClick(int position);
    }

    public ListAdapter(List<ListItem> items, ItemClickCallback itemClickCallback) {
        mItems = items;
        mItemClickCallback = itemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Context context = holder.itemView.getContext();

        Animation radAnim = AnimationUtils.loadAnimation(context, R.anim.rad_circle_rotate);
        ListItem item = mItems.get(position);

        String title = getString(context, item.getName());

        holder.mTitle.setText(title);
        if (item.isPlaying()) {
            if (holder.mPlayingStatus.getAnimation() == null) {
                holder.mPlayingStatus.startAnimation(radAnim);
            }
            // Keeping the screen on while playing a sound
            holder.mContainer.setKeepScreenOn(true);
        } else {
            if (holder.mPlayingStatus.getAnimation() != null) {
                holder.mPlayingStatus.setAnimation(null);
            }
            // Release keeping the screen on
            holder.mContainer.setKeepScreenOn(false);
        }
        if (position > 0) {
            holder.mFavoriteStatus.setVisibility(View.VISIBLE);
            if (item.isFavorite()) {
                holder.mFavoriteStatus.setImageResource(R.drawable.ic_star);
            } else {
                holder.mFavoriteStatus.setImageResource(R.drawable.ic_star_border);
            }
        } else {
            holder.mFavoriteStatus.setVisibility(View.INVISIBLE);
        }
        if (position > 0) {
            holder.mContainer.setLongClickable(true);
        } else {
            holder.mContainer.setLongClickable(false);
        }
        holder.mContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (holder.getAdapterPosition() != 0) {
                    mItemClickCallback.onContextMenuClick(holder.getAdapterPosition());
                }
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        if (holder.mPlayingStatus.getAnimation() != null)
            holder.mPlayingStatus.getAnimation().start();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView mTitle;
        private ImageView mPlayingStatus;
        private ImageView mFavoriteStatus;
        private View mContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.lbl_item_text);
            mPlayingStatus = (ImageView) itemView.findViewById(R.id.stts_item_icon);
            mFavoriteStatus = (ImageView) itemView.findViewById(R.id.fav_item_icon);
            mFavoriteStatus.setOnClickListener(this);
            mContainer = itemView.findViewById(R.id.cont_item_root);
            mContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.fav_item_icon)
                mItemClickCallback.onFavoriteClick(getAdapterPosition());
            else
                mItemClickCallback.onPlayClick(getAdapterPosition());
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_header, parent, false);
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderViewHolder holder, int position) {
        Context context = holder.itemView.getContext();

        Resources resources = context.getResources();
        String packageName = context.getPackageName();
        String subtitle = context.getString(
                resources.getIdentifier(mItems.get(position).getSubtitle(), "string", packageName));

        holder.mSubtitle.setText(subtitle);
    }

    @Override
    public long getHeaderId(int position) {
        return mItems.get(position).getSubtitlePosition();
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView mSubtitle;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            mSubtitle = (TextView) itemView.findViewById(R.id.header_text);
        }
    }
}
