package com.blinkseven.banditboard.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blinkseven.banditboard.BuildConfig;
import com.blinkseven.banditboard.R;

public class AboutActivity extends AppCompatActivity {

    private static final String MESSAGE = " cdn.efukt.com/2014/08/55f0705d270e_efukt.gif";
    private int count = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_about);
        super.onCreate(savedInstanceState);

        // Set app version
        String versionName = BuildConfig.VERSION_NAME;
        TextView versionTextView = (TextView) findViewById(R.id.version_text_view);
        versionTextView.setText(getString(R.string.version, versionName));

        // Set writings listeners
        TextView textViewOSL = (TextView) findViewById(R.id.osl_text_view);
        textViewOSL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOSL();
            }
        });

        // Make links to 'Developed by' and 'Arts by'
        TextView textViewDB = (TextView) findViewById(R.id.developed_by_text_view);
        textViewDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://google.com/+AlexanderKraskovsky")));
            }
        });
        TextView textViewAB = (TextView) findViewById(R.id.arts_by_text_view);
        textViewAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://vk.com/id273913463")));
            }
        });

        // Make Logo bubble
        ImageView imageViewLogo = (ImageView) findViewById(R.id.logo_image_view);
        imageViewLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bubbleLogo(v);
                showEgg();
            }
        });
    }

    private void showOSL() {
        WebView view = (WebView) LayoutInflater.from(this).inflate(R.layout.dialog_licenses, null);
        view.loadUrl("file:///android_asset/open_source_licenses.html");

        AlertDialog.Builder builder
                = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setTitle("Open source licenses");
        builder.setView(view);
        builder.setPositiveButton(android.R.string.ok, null);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showEgg() {
        if (count == MESSAGE.length() * 3) {
            count = 0;
            return;
        }

        if (count != 0 && count % 3 == 0) {
            Toast.makeText(this, String.valueOf(MESSAGE.charAt(count / 3)), Toast.LENGTH_SHORT)
                    .show();
        }
        count++;
    }

    private void bubbleLogo(View view) {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        myAnim.setInterpolator(new MyBounceInterpolator(0.2, 20));

        view.startAnimation(myAnim);
    }

    private class MyBounceInterpolator implements Interpolator {
        double mAmplitude = 1;
        double mFrequency = 10;

        MyBounceInterpolator(double amplitude, double frequency) {
            mAmplitude = amplitude;
            mFrequency = frequency;
        }

        public float getInterpolation(float time) {
            return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                    Math.cos(mFrequency * time) + 1);
        }
    }
}
