package com.blinkseven.banditboard.ui;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.blinkseven.banditboard.R;
import com.blinkseven.banditboard.data.BanditContract.BaseEntry;
import com.blinkseven.banditboard.model.FilesResolver;
import com.blinkseven.banditboard.model.ListCallback;
import com.blinkseven.banditboard.model.FavoriteListCallback;
import com.blinkseven.banditboard.model.SoundPlayer;
import com.blinkseven.banditboard.model.adapter.ListAdapter;
import com.blinkseven.banditboard.model.item.ListItem;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.yqritc.recyclerviewflexibledivider.FlexibleDividerDecoration;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class ListFragment
        extends Fragment
        implements ListAdapter.ItemClickCallback,
        ListCallback {

    public static final String LOG_TAG = ListFragment.class.getSimpleName();

    private static final String BUNDLE_TABLE_NAME = "TABLE_NAME";
    private static final String BUNDLE_TABLE_URI = "TABLE_URI";

    private ContextMenuRecyclerView recyclerView;

    private final List<ListItem> mItems = new ArrayList<>();
    private ListAdapter mAdapter;
    private SoundPlayer mPlayer;

    private ViewGroup emptyView;

    public static ListFragment newInstance(String tableName, Uri tableUri) {
        ListFragment f = new ListFragment();

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString(BUNDLE_TABLE_NAME, tableName);
        args.putString(BUNDLE_TABLE_URI, tableUri.toString());
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            ((MainActivity) getActivity()).addOnFavoriteListener(this);
        } catch (Exception e) {
            throw new ClassCastException(context.toString() + " must implement ListCallback");
        }
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_sound_list, container, false);

        emptyView = (ViewGroup) rootView.findViewById(R.id.empty_view);
        emptyView.setVisibility(View.GONE);

        recyclerView = (ContextMenuRecyclerView) rootView.findViewById(R.id.rec_list);
        recyclerView.setHasFixedSize(true);
        RecyclerView.ItemAnimator animator = recyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        registerForContextMenu(recyclerView);

        loadDatabase();
        initAdapter();
        initPlayer();

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new StickyRecyclerHeadersDecoration(mAdapter));

        setupRecyclerViewDivider();

        return rootView;
    }

    private void loadDatabase() {
        if (!mItems.isEmpty())
            return;

        String[] projection = {
                BaseEntry.COLUMN_NAME,
                BaseEntry.COLUMN_FAVORITE,
                BaseEntry.COLUMN_SUBTITLE,
                BaseEntry.COLUMN_SUBTITLE_POSITION};

        Cursor cursor = getContext().getContentResolver().query(
                Uri.parse(getArguments().getString(BUNDLE_TABLE_URI)),
                projection,
                null, null, null);

        int nameColumnIndex = cursor.getColumnIndex(BaseEntry.COLUMN_NAME);
        int favoriteColumnIndex = cursor.getColumnIndex(BaseEntry.COLUMN_FAVORITE);
        int subtitleColumnIndex = cursor.getColumnIndex(BaseEntry.COLUMN_SUBTITLE);
        int subtitlePositionColumnIndex = cursor.getColumnIndex(BaseEntry.COLUMN_SUBTITLE_POSITION);

        try {
            while (cursor.moveToNext()) {
                String name = cursor.getString(nameColumnIndex);
                boolean favorite = cursor.getInt(favoriteColumnIndex) == BaseEntry.FAVORITE;
                String subtitle = cursor.getString(subtitleColumnIndex);
                int subtitlePosition = cursor.getInt(subtitlePositionColumnIndex);

                ListItem item = new ListItem(name, favorite, subtitle, subtitlePosition);
                mItems.add(item);
            }
        } finally {
            cursor.close();
        }
    }

    private void initAdapter() {
        if (mAdapter != null)
            return;

        mAdapter = new ListAdapter(mItems, this);
    }

    private void initPlayer() {
        if (mPlayer != null)
            return;

        mPlayer = new SoundPlayer(getContext(), mAdapter, mItems);
    }

    private void setupRecyclerViewDivider() {
        // Hide a divider under a header
        FlexibleDividerDecoration.VisibilityProvider visibilityProvider
                = new FlexibleDividerDecoration.VisibilityProvider() {
            @Override
            public boolean shouldHideDivider(int position, RecyclerView parent) {
                if (mItems.get(position).getSubtitlePosition()
                        != mItems.get(position + 1).getSubtitlePosition()) {
                    return true;
                }
                return false;
            }
        };

        // Make a divider decoration
        HorizontalDividerItemDecoration horizontalDivider
                = new HorizontalDividerItemDecoration.Builder(getContext())
                .marginResId(R.dimen.divider_left_margin, R.dimen.divider_right_margin)
                .visibilityProvider(visibilityProvider)
                .build();

        // Set the divider decoration to the recycler view
        recyclerView.addItemDecoration(horizontalDivider);
    }

    @Override
    public void onPlayClick(int position) {
        mPlayer.onPlayClick(position);
    }

    @Override
    public void onFavoriteClick(int position) {
        FavoriteListCallback listener = ((MainActivity) getActivity()).getOnListListener();

        ListItem item = mItems.get(position);
        if (!item.isFavorite()) {
            changeFavoriteStateLocal(position, true);
            changeFavoriteStateDb(position, true);

            listener.onItemAdded(getArguments().getString(BUNDLE_TABLE_NAME), item.getName());
        } else {
            changeFavoriteStateLocal(position, false);
            changeFavoriteStateDb(position, false);

            listener.onItemRemoved(item.getName());
        }
    }

    @Override
    public void onItemRemoved(String tableName, final String itemName) {
        if (!tableName.equals(getArguments().getString(BUNDLE_TABLE_NAME)))
            return;

        for (int i = 0; i < mItems.size(); i++) {
            ListItem item = mItems.get(i);

            if (item.getName().equals(itemName)) {
                changeFavoriteStateLocal(i, false);
                changeFavoriteStateDb(i, false);
                break;
            }
        }
    }

    private void changeFavoriteStateLocal(int position, boolean state) {
        ListItem item = mItems.get(position);
        item.setFavorite(state);
        mAdapter.notifyItemChanged(position);
    }

    private void changeFavoriteStateDb(final int position, final boolean state) {
        new Thread() {
            @Override
            public void run() {
                ListItem item = mItems.get(position);

                ContentValues values = new ContentValues();
                values.put(
                        BaseEntry.COLUMN_FAVORITE,
                        state ? BaseEntry.FAVORITE : BaseEntry.NOT_FAVORITE);

                String selection = BaseEntry.COLUMN_NAME + " = ?";
                String[] selectionArgs = {item.getName()};

                getContext().getContentResolver().update(
                        Uri.parse(getArguments().getString(BUNDLE_TABLE_URI)),
                        values,
                        selection,
                        selectionArgs);
            }
        }.start();
    }

    @Override
    public void onReleasePlayer() {
        if (mPlayer != null)
            mPlayer.releaseMediaPlayer();
    }

    @Override
    public void onContextMenuClick(int position) {
        recyclerView.openContextMenu(position);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        /*super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.item_context_menu, menu);*/
        int uniqueId = getArguments().getString(BUNDLE_TABLE_NAME).hashCode();
        menu.add(uniqueId, R.id.share, 0, R.string.share);
        menu.add(uniqueId, R.id.download, 0, R.string.download);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int uniqueId = getArguments().getString(BUNDLE_TABLE_NAME).hashCode();
        if (item.getGroupId() != uniqueId) {
            return false;
        }

        ContextMenuRecyclerView.RecyclerContextMenuInfo info
                = (ContextMenuRecyclerView.RecyclerContextMenuInfo) item.getMenuInfo();

        String name = mItems.get(info.position).getName();
        FilesResolver resolver = new FilesResolver(getContext());
        switch (item.getItemId()) {
            case R.id.share:
                resolver.share(name);
                return true;
            case R.id.download:
                resolver.download(name);
                return true;
            default:
                return true;
        }
    }
}
