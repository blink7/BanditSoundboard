package com.blinkseven.banditboard.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.blinkseven.banditboard.R;
import com.blinkseven.banditboard.service.MusicService;

public class SoundBoxFragment extends Fragment implements View.OnClickListener, MusicService.CallbackListener {
    public static final String LOG_CAT = SoundBoxFragment.class.getSimpleName();

    private ImageButton mPlayPauseButton;
    private ImageButton mStopButton;
    private ImageButton mLoopButton;
    private ProgressBar mProgressBar;

    private MusicService mMusicService;
    private Intent playIntent;

    // Handler to update UI timer, progress bar etc,.
    private Handler mHandler = new Handler();

    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.PlayerBinder binder = (MusicService.PlayerBinder) service;

            // Get service
            mMusicService = binder.getService();
            updateProgressBar();
            mMusicService.setListener(SoundBoxFragment.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    @Override
    public void onStart() {
        super.onStart();

        // Start service
        startMusicService();
    }

    private void startMusicService() {
        if (playIntent == null) {
            playIntent = new Intent(getContext(), MusicService.class);
            getActivity().bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sound_box, container, false);

        // Define all buttons and the progress bar
        mPlayPauseButton = (ImageButton) rootView.findViewById(R.id.play_button);
        mStopButton = (ImageButton) rootView.findViewById(R.id.stop_button);
        mLoopButton = (ImageButton) rootView.findViewById(R.id.loop_button);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        mProgressBar.setMax(1000);

        // Set button listeners
        mPlayPauseButton.setOnClickListener(this);
        mStopButton.setOnClickListener(this);
        mLoopButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v == mPlayPauseButton) {
            getActivity().startService(new Intent(getContext(), MusicService.class)
                    .setAction(MusicService.ACTION_PLAY));
        } else if (v == mStopButton) {
            getActivity().startService(new Intent(getContext(), MusicService.class)
                    .setAction(MusicService.ACTION_STOP));
        } else if (v == mLoopButton) {
            getActivity().startService(new Intent(getContext(), MusicService.class)
                    .setAction(MusicService.ACTION_LOOP));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mMusicService != null) {
            outState.putInt(MusicService.ACTION_PLAY, mMusicService.getState());
            outState.putInt(MusicService.ACTION_LOOP, mMusicService.getLoopState());
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {
            updatePlaybackState(savedInstanceState.getInt(MusicService.ACTION_PLAY));
            updateLoopState(savedInstanceState.getInt(MusicService.ACTION_LOOP));
        }
    }

    @Override
    public void onDestroy() {
        // Remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);

        getActivity().unbindService(musicConnection);

        if (getActivity().isFinishing()) {
            // Stop service
            getActivity().stopService(playIntent);
            mMusicService = null;
        }

        super.onDestroy();
    }

    @Override
    public void updatePlaybackState(int state) {
        if (state == MusicService.STATE_PLAYING) {
            mPlayPauseButton.setImageResource(R.drawable.ic_pause);
            mPlayPauseButton.setContentDescription(
                    getContext().getString(R.string.sound_box_pause_description));
        } else {
            mPlayPauseButton.setImageResource(R.drawable.ic_play);
            mPlayPauseButton.setContentDescription(
                    getContext().getString(R.string.sound_box_play_description));
        }
    }

    @Override
    public void updateLoopState(int state) {
        if (state == MusicService.STATE_LOOP_ON) {
            mLoopButton.setImageResource(R.drawable.ic_loop_pressed);
            mLoopButton.setContentDescription(
                    getContext().getString(R.string.sound_box_loop_off_description));
        } else {
            mLoopButton.setImageResource(R.drawable.ic_loop_default);
            mLoopButton.setContentDescription(
                    getContext().getString(R.string.sound_box_loop_on_description));
        }
    }

    @Override
    public void updateProgressBar() {
        mProgressBar.setMax(mMusicService.getDuration() / 100);
        mHandler.post(mUpdateTimeTask);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            // Updating progress bar
            mProgressBar.setProgress(mMusicService.getCurrentPosition() / 100);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 200);
        }
    };
}
