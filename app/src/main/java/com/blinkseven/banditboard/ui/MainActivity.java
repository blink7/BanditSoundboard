package com.blinkseven.banditboard.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.blinkseven.banditboard.R;
import com.blinkseven.banditboard.model.FavoriteListCallback;
import com.blinkseven.banditboard.model.ListCallback;
import com.blinkseven.banditboard.model.adapter.PagerAdapter;
import com.blinkseven.banditboard.util.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements DialogInterface.OnDismissListener {
    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    private ViewPager mViewPager;

    private List<ListCallback> mListCallbacks = new ArrayList<>();
    private FavoriteListCallback mFavoriteListCallback;

    private InterstitialAd mInterstitialAd;

    private View fragmentSoundBox;
    private boolean animationStarted = false;

    private int screenPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentSoundBox = findViewById(R.id.fragment_sound_box);
        if (!animationStarted) {
            fragmentSoundBox.setVisibility(View.INVISIBLE);
        }

        // Create an adapter and add fragments
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), this);

        // Create a ViewPager
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(adapter);

        SmartTabLayout smartTabLayout = (SmartTabLayout) findViewById(R.id.viewpagertab);
        smartTabLayout.setViewPager(mViewPager);

        CollapsingToolbarLayout collapsingLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_layout);
        collapsingLayout.setTitleEnabled(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_tool_bar);
        setSupportActionBar(toolbar);

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Utils.pxToDp(getApplicationContext(),
                        appBarLayout.getTotalScrollRange() + verticalOffset) <= 80) {
                    getSupportActionBar().setDisplayShowTitleEnabled(true);
                } else {
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                }
            }
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-5471413908979911/2948012188");

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    if ((new Random().nextInt(4)) == 0 && mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }

                if (position == 0 && screenPos != 0) {
                    changeImageView(R.drawable.forest_1280x720);
                    screenPos = 0;
                } else if (position != 0 && screenPos == 0){
                    changeImageView(R.drawable.yard_1280x720);
                    screenPos = position;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice("0411307A574D1F33EEA21F71B549C45E")
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    public void changeImageView(final int resId) {
        final ImageView toolbarImageView = (ImageView) findViewById(R.id.toolbar_image_view);

        final Animation anim_out = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        final Animation anim_in = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        anim_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                toolbarImageView.setImageResource(resId);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                });
                toolbarImageView.startAnimation(anim_in);
            }
        });
        toolbarImageView.startAnimation(anim_out);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (!hasFocus || animationStarted) {
            return;
        }

        showUpSoundBox();

        getPermissionToWriteExternal();

        super.onWindowFocusChanged(hasFocus);
    }

    private void showUpSoundBox() {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.bottom_up);
        fragmentSoundBox.startAnimation(bottomUp);
        fragmentSoundBox.setVisibility(View.VISIBLE);
        animationStarted = true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem menuItem = menu.findItem(R.id.delete_all_favorite);
        if (mViewPager.getCurrentItem() != 0 || mFavoriteListCallback.isEmpty()) {
            menuItem.setVisible(false);
        } else {
            menuItem.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_context_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all_favorite:
                showDeleteConfirmationDialog();
                return true;
            case R.id.about:
                startAboutActivity();
                return true;
            case R.id.exit:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startAboutActivity() {
        Intent intent = new Intent(MainActivity.this, AboutActivity.class);
        startActivity(intent);
    }

    /**
     * Prompt the user to confirm that they want to erase favorite list.
     */
    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setMessage(R.string.delete_all_message);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the list.
                mFavoriteListCallback.onClearList();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mFavoriteListCallback.onReleasePlayer();
        for (ListCallback listener : mListCallbacks) {
            listener.onReleasePlayer();
        }
    }

    public List<ListCallback> getOnFavoriteListeners() {
        return mListCallbacks;
    }

    public void addOnFavoriteListener(ListCallback listCallback) {
        mListCallbacks.add(listCallback);
    }

    public FavoriteListCallback getOnListListener() {
        return mFavoriteListCallback;
    }

    public void setOnListListener(FavoriteListCallback favoriteListCallback) {
        mFavoriteListCallback = favoriteListCallback;
    }

    private void getPermissionToWriteExternal() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    new WriteExternalDialogFragment().show(getFragmentManager(), "DialogFragment");
                }
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void onContextMenuClosed(Menu menu) {
        super.onContextMenuClosed(menu);

        ((FavoriteFragment) mFavoriteListCallback).onContextMenuClosed();
    }
}
