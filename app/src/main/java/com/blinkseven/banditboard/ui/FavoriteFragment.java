package com.blinkseven.banditboard.ui;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.blinkseven.banditboard.R;
import com.blinkseven.banditboard.data.BanditContract;
import com.blinkseven.banditboard.data.BanditContract.FavoriteEntry;
import com.blinkseven.banditboard.model.FilesResolver;
import com.blinkseven.banditboard.model.FavoriteListCallback;
import com.blinkseven.banditboard.model.ListCallback;
import com.blinkseven.banditboard.model.SoundPlayer;
import com.blinkseven.banditboard.model.adapter.FavoriteAdapter;
import com.blinkseven.banditboard.model.item.FavoriteItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.blinkseven.banditboard.data.BanditContract.BaseEntry.NOT_FAVORITE;

public class FavoriteFragment
        extends Fragment
        implements FavoriteAdapter.ItemClickCallback, FavoriteListCallback {

    public static final String LOG_TAG = FavoriteFragment.class.getSimpleName();

    private static final int UNIQUE_FRAGMENT_GROUP_ID = 1;

    private ContextMenuRecyclerView recyclerView;

    private List<FavoriteItem> mItems = new ArrayList<>();
    private FavoriteAdapter mAdapter;
    private ItemTouchHelper mItemTouchHelper;
    private SoundPlayer mPlayer;

    private AtomicBoolean mAllowSwipe = new AtomicBoolean(true);

    private ViewGroup emptyView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            ((MainActivity) getActivity()).setOnListListener(this);
        } catch (Exception e) {
            throw new ClassCastException(context.toString() + " must implement FavoriteListCallback");
        }
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);

        View rootView = inflater.inflate(R.layout.fragment_sound_list, container, false);

        emptyView = (ViewGroup) rootView.findViewById(R.id.empty_view);

        recyclerView = (ContextMenuRecyclerView) rootView.findViewById(R.id.rec_list);
        RecyclerView.ItemAnimator animator = recyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        registerForContextMenu(recyclerView);

        loadDatabase();
        initAdapter();
        initPlayer();
        checkEmptyView();

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mItemTouchHelper = new ItemTouchHelper(createHelperCallback());
        mItemTouchHelper.attachToRecyclerView(recyclerView);

        return rootView;
    }

    private void loadDatabase() {
        if (!mItems.isEmpty())
            return;

        String[] projection = {
                FavoriteEntry.COLUMN_FAVORITE_NAME,
                FavoriteEntry.COLUMN_FAVORITE_TABLE};
        Cursor cursor = getContext().getContentResolver().query(
                FavoriteEntry.CONTENT_URI,
                projection,
                null, null, null);

        try {
            int nameColumnIndex = cursor.getColumnIndex(FavoriteEntry.COLUMN_FAVORITE_NAME);
            int tableColumnIndex = cursor.getColumnIndex(FavoriteEntry.COLUMN_FAVORITE_TABLE);

            while (cursor.moveToNext()) {
                String currentName = cursor.getString(nameColumnIndex);
                String currentTable = cursor.getString(tableColumnIndex);

                FavoriteItem item = new FavoriteItem(currentName, currentTable);
                mItems.add(item);
            }
        } finally {
            cursor.close();
        }
    }

    private void initAdapter() {
        if (mAdapter != null)
            return;

        mAdapter = new FavoriteAdapter(mItems, this);
    }

    private void initPlayer() {
        if (mPlayer != null)
            return;

        mPlayer = new SoundPlayer(getContext(), mAdapter, mItems);
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(
                        ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean isLongPressDragEnabled() {
                        return false;
                    }

                    @Override
                    public boolean isItemViewSwipeEnabled() {
                        return mAllowSwipe.get();
                    }

                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                        return true;
                    }

                    @Override
                    public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        FavoriteItem item = mItems.get(viewHolder.getAdapterPosition());

                        showSnackbar(viewHolder.getAdapterPosition(), item);
                        mPlayer.releaseMediaPlayer();
                        mItems.remove(viewHolder.getAdapterPosition());
                        mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                        checkEmptyView();
                    }
                };
        return simpleItemTouchCallback;
    }

    private void moveItem(final int oldPos, final int newPos) {
        final FavoriteItem item = mItems.get(oldPos);
        mItems.remove(oldPos);
        mItems.add(newPos, item);
        mAdapter.notifyItemMoved(oldPos, newPos);

        new Thread() {
            @Override
            public void run() {
                String[] projection = {
                        FavoriteEntry._ID,
                        FavoriteEntry.COLUMN_FAVORITE_NAME,
                        FavoriteEntry.COLUMN_FAVORITE_TABLE};
                Cursor cursor = getContext().getContentResolver().query(
                        FavoriteEntry.CONTENT_URI,
                        projection,
                        null, null, null);

                try {
                    int idColumnIndex = cursor.getColumnIndex(FavoriteEntry._ID);
                    int nameColumnIndex = cursor.getColumnIndex(FavoriteEntry.COLUMN_FAVORITE_NAME);
                    int tableColumnIndex = cursor.getColumnIndex(FavoriteEntry.COLUMN_FAVORITE_TABLE);

                    cursor.moveToPosition(oldPos);
                    long oldId = cursor.getLong(idColumnIndex);
                    String currentName = cursor.getString(nameColumnIndex);
                    String currentTable = cursor.getString(tableColumnIndex);

                    cursor.moveToPosition(newPos);
                    long newId = cursor.getLong(idColumnIndex);

                    Uri uriWithOldId = ContentUris.withAppendedId(FavoriteEntry.CONTENT_URI, oldId);
                    ContentValues values = new ContentValues();
                    values.put(FavoriteEntry._ID, newId);
                    values.put(FavoriteEntry.COLUMN_FAVORITE_NAME, currentName);
                    values.put(FavoriteEntry.COLUMN_FAVORITE_TABLE, currentTable);
                    getContext().getContentResolver().insert(uriWithOldId, values);
                } finally {
                    cursor.close();
                }
            }
        }.start();
    }

    private void showSnackbar(final int position, final FavoriteItem item) {
        Snackbar mySnackbar = Snackbar.make(
                getActivity().findViewById(R.id.activity_main),
                R.string.item_removed,
                Snackbar.LENGTH_SHORT);

        mySnackbar.setAction(R.string.undo, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItems.add(position, item);
                mAdapter.notifyItemInserted(position);
                checkEmptyView();
            }
        });

        mySnackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                if (event == Snackbar.Callback.DISMISS_EVENT_TIMEOUT
                        || event == Snackbar.Callback.DISMISS_EVENT_CONSECUTIVE) {
                    deleteItem(item);
                }
            }
        });

        mySnackbar.show();
    }

    private void deleteItem(final FavoriteItem item) {
        new Thread() {
            @Override
            public void run() {
                String name = item.getName();
                String tableSrc = item.getTable();

                Uri uri;
                switch (tableSrc) {
                    case BanditContract.PhraseEntry.TABLE_NAME:
                        uri = BanditContract.PhraseEntry.CONTENT_URI;
                        break;
                    case BanditContract.IdleEntry.TABLE_NAME:
                        uri = BanditContract.IdleEntry.CONTENT_URI;
                        break;
                    case BanditContract.JokeEntry.TABLE_NAME:
                        uri = BanditContract.JokeEntry.CONTENT_URI;
                        break;
                    default:
                        return;
                }

                ContentValues values = new ContentValues();
                values.put(BanditContract.BaseEntry.COLUMN_FAVORITE, NOT_FAVORITE);
                String selection = BanditContract.BaseEntry.COLUMN_NAME + " = ?";
                String[] selectionArgs = {name};

                // Update (delete favorite mark) in all lists
                getContext().getContentResolver().update(uri, values, selection, selectionArgs);

                // Delete the item from Favorite DB
                uri = FavoriteEntry.CONTENT_URI;
                getContext().getContentResolver().delete(uri, selection, selectionArgs);

            }
        }.start();

        for (ListCallback listener : ((MainActivity) getActivity()).getOnFavoriteListeners()) {
            listener.onItemRemoved(item.getTable(), item.getName());
        }
    }

    @Override
    public void onPlayClick(int position) {
        mPlayer.onPlayClick(position);
    }

    @Override
    public void onStartDrag(FavoriteAdapter.ViewHolder holder) {
        mPlayer.releaseMediaPlayer();

        mItemTouchHelper.startDrag(holder);
    }

    @Override
    public void onItemAdded(final String tableName, final String itemName) {
        mItems.add(new FavoriteItem(itemName, tableName));
        mAdapter.notifyDataSetChanged();

        checkEmptyView();

        new Thread() {
            @Override
            public void run() {
                ContentValues values = new ContentValues();
                values.put(FavoriteEntry.COLUMN_FAVORITE_NAME, itemName);
                values.put(FavoriteEntry.COLUMN_FAVORITE_TABLE, tableName);
                getContext().getContentResolver().insert(FavoriteEntry.CONTENT_URI, values);
            }
        }.start();
    }

    @Override
    public void onItemRemoved(final String itemName) {
        mPlayer.releaseMediaPlayer();

        Iterator<FavoriteItem> iterator = mItems.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getName().equals(itemName)) {
                iterator.remove();
                break;
            }
        }
        mAdapter.notifyDataSetChanged();

        checkEmptyView();

        new Thread() {
            @Override
            public void run() {
                String selection = FavoriteEntry.COLUMN_FAVORITE_NAME + " = ?";
                String[] selectionArgs = {itemName};
                getContext().getContentResolver().delete(FavoriteEntry.CONTENT_URI, selection, selectionArgs);
            }
        }.start();
    }

    @Override
    public void onClearList() {
        mPlayer.releaseMediaPlayer();

        int size = mItems.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                FavoriteItem item = mItems.get(0);
                deleteItem(item);

                mItems.remove(0);
            }
            mAdapter.notifyItemRangeRemoved(0, size);
        }

        checkEmptyView();
    }

    @Override
    public boolean isEmpty() {
        return mItems.isEmpty();
    }

    @Override
    public void onReleasePlayer() {
        if (mPlayer != null)
            mPlayer.releaseMediaPlayer();
    }

    @Override
    public void onContextMenuClick(int position) {
        mAllowSwipe.set(false);
        recyclerView.openContextMenu(position);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(UNIQUE_FRAGMENT_GROUP_ID, R.id.share, 0, R.string.share);
        menu.add(UNIQUE_FRAGMENT_GROUP_ID, R.id.download, 0, R.string.download);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getGroupId() != UNIQUE_FRAGMENT_GROUP_ID) {
            return false;
        }

        ContextMenuRecyclerView.RecyclerContextMenuInfo info
                = (ContextMenuRecyclerView.RecyclerContextMenuInfo) item.getMenuInfo();

        String name = mItems.get(info.position).getName();
        FilesResolver resolver = new FilesResolver(getContext());
        switch (item.getItemId()) {
            case R.id.share:
                resolver.share(name);
                return true;
            case R.id.download:
                resolver.download(name);
                return true;
            default:
                return true;
        }
    }

    private void checkEmptyView() {
        if (mItems.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
            Animation fadeInAnim = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            emptyView.startAnimation(fadeInAnim);
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    public void onContextMenuClosed() {
        mAllowSwipe.set(true);
    }
}
