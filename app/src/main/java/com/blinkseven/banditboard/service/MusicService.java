package com.blinkseven.banditboard.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v7.app.NotificationCompat;
import android.widget.RemoteViews;

import com.blinkseven.banditboard.R;
import com.blinkseven.banditboard.ui.MainActivity;

public class MusicService extends Service implements MediaPlayer.OnCompletionListener {
    public static final String LOG_CAT = MusicService.class.getSimpleName();

    private MediaPlayer mPlayer;
    private Notification mNotification;

    private NotificationManager notificationManager;
    private RemoteViews notificationView;

    private final IBinder musicBinder = new PlayerBinder();

    private int mState = STATE_STOPPED;
    private int mLoopState;

    private CallbackListener mListener;

    public static final String ACTION_PLAY = "com.blinkseven.banditboard.ACTION_PLAY";
    public static final String ACTION_STOP = "com.blinkseven.banditboard.ACTION_STOP";
    public static final String ACTION_LOOP = "com.blinkseven.banditboard.ACTION_LOOP";

    public static final int STATE_STOPPED = 0;
    public static final int STATE_PAUSED = 1;
    public static final int STATE_PLAYING = 2;
    public static final int STATE_LOOP_ON = 4;
    public static final int STATE_LOOP_OFF = 8;

    private static final int REQUEST_CODE_PAUSE = 101;
    private static final int REQUEST_CODE_STOP = 102;
    private static final int REQUEST_CODE_LOOP = 104;

    public static final int NOTIFICATION_ID = 11;

    private boolean isNotificationVisible;

    public class PlayerBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    void createMediaPlayerIfNeeded() {
        if (mPlayer == null) {
            mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.garbage_bandits_radio_1);
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            // Make sure the media player will acquire a wake-lock while playing. If we don't do
            // that, the CPU might go to sleep while the song is playing, causing playback to stop.
            //
            // Remember that to use this, we have to declare the android.permission.WAKE_LOCK
            // permission in AndroidManifest.xml.
            mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);

            // we want the media player to notify us when it's done playing:
            mPlayer.setOnCompletionListener(this);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        switch (action) {
            case ACTION_PLAY:
                processPlayPauseRequest();
                break;
            case ACTION_STOP:
                processStopRequest();
                break;
            case ACTION_LOOP:
                processLoopRequest();
                break;
        }

        return START_NOT_STICKY; // Means we started the service, but don't want it to
                                // restart in case it's killed.
    }

    public void processPlayPauseRequest() {
        if (mState == STATE_STOPPED) {
            createMediaPlayerIfNeeded();
            mState = STATE_PLAYING;
            mPlayer.start();
            showNotification();
            if (mListener != null) {
                mListener.updateProgressBar();
            }
        } else if (mState == STATE_PAUSED) {
            mState = STATE_PLAYING;
            if (!mPlayer.isPlaying()) mPlayer.start();
            updateNotification();
        } else if (mState == STATE_PLAYING){
            mState = STATE_PAUSED;
            mPlayer.pause();
            updateNotification();
        }

        if (mListener != null) {
            mListener.updatePlaybackState(mState);
        }
    }

    public void processStopRequest() {
        if (mState == STATE_PLAYING || mState == STATE_PAUSED) {
            mState = STATE_STOPPED;

            relaxResources(false);

            mPlayer.pause();
            mPlayer.seekTo(0);
            if (mListener != null) {
                mListener.updatePlaybackState(mState);
            }
            notificationManager.cancel(NOTIFICATION_ID);
            isNotificationVisible = false;
        }
    }

    public void processLoopRequest() {
        createMediaPlayerIfNeeded();

        if (mPlayer.isLooping()) {
            mLoopState = STATE_LOOP_OFF;
            mPlayer.setLooping(false);
        } else {
            mLoopState = STATE_LOOP_ON;
            mPlayer.setLooping(true);
        }

        if (isNotificationVisible) {
            updateNotification();
        }

        if (mListener != null) {
            mListener.updateLoopState(mLoopState);
        }
    }

    public int getDuration() {
        return mPlayer != null ? mPlayer.getDuration() : 0;
    }

    public int getCurrentPosition() {
        return mPlayer != null ? mPlayer.getCurrentPosition() : 0;
    }

    public int getState() {
        return mState;
    }

    public int getLoopState() {
        return mLoopState;
    }

    @SuppressLint("NewApi")
    private void showNotification() {
        PendingIntent pendingIntent;
        Intent intent;
        notificationView = new RemoteViews(getPackageName(), R.layout.notification_mediacontroller);

        int api = Build.VERSION.SDK_INT;

        // If API is lower 21 level (android 5.0 lollipop) make a white background on the notification
        if (api < 21) {
            notificationView.setInt(R.id.notify_container, "setBackgroundColor", Color.WHITE);
        }

        // Intent to stop action
        intent = new Intent(ACTION_STOP);
        pendingIntent = PendingIntent.getService(
                getApplicationContext(),
                REQUEST_CODE_STOP,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        notificationView.setOnClickPendingIntent(R.id.notify_btn_stop, pendingIntent);

        // Intent to play/pause action
        intent = new Intent(ACTION_PLAY);
        pendingIntent = PendingIntent.getService(
                getApplicationContext(),
                REQUEST_CODE_PAUSE,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        notificationView.setOnClickPendingIntent(R.id.notify_btn_play_pause, pendingIntent);

        // Intent to loop action
        intent = new Intent(ACTION_LOOP);
        pendingIntent = PendingIntent.getService(
                getApplicationContext(),
                REQUEST_CODE_LOOP,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        notificationView.setOnClickPendingIntent(R.id.notify_btn_loop, pendingIntent);

        // Intent to resume the app
        intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(
                getApplicationContext(),
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Build the notification. If API is higher 23 level build through Notification.Builder
        // and set the special style
        if (api < 24) {
            mNotification = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.ic_notify_radio)
                    .setWhen(System.currentTimeMillis())
                    .setContent(notificationView)
                    .setDefaults(Notification.FLAG_AUTO_CANCEL)
                    .setContentIntent(pendingIntent)
                    .build();
        } else {
            mNotification = new Notification.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.ic_notify_radio)
                    .setWhen(System.currentTimeMillis())
                    .setCustomContentView(notificationView)
                    .setStyle(new Notification.DecoratedMediaCustomViewStyle())
                    .setDefaults(Notification.FLAG_AUTO_CANCEL)
                    .setContentIntent(pendingIntent)
                    .build();
        }

        notificationManager.notify(NOTIFICATION_ID, mNotification);
        isNotificationVisible = true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        processStopRequest();
    }

    private void updateNotification() {
        if (mState == STATE_PLAYING) {
            notificationView.setImageViewResource(R.id.notify_btn_play_pause, R.drawable.ic_notify_pause);
        } else {
            notificationView.setImageViewResource(R.id.notify_btn_play_pause, R.drawable.ic_notify_play);
        }

        if (mLoopState == STATE_LOOP_ON) {
            notificationView.setImageViewResource(R.id.notify_btn_loop, R.drawable.ic_notify_loop_pressed);
        } else {
            notificationView.setImageViewResource(R.id.notify_btn_loop, R.drawable.ic_notify_loop_default);
        }

        notificationManager.notify(NOTIFICATION_ID, mNotification);
    }

    public interface CallbackListener {
        void updatePlaybackState(int state);
        void updateLoopState(int state);
        void updateProgressBar();
    }

    public void setListener(CallbackListener listener) {
        this.mListener = listener;
    }

    /**
     * Releases resources used by the service for playback. This includes the notification
     * and possibly the MediaPlayer.
     *
     * @param releaseMediaPlayer Indicates whether the Media Player should also be released or not
     */
    void relaxResources(boolean releaseMediaPlayer) {
        notificationManager.cancel(NOTIFICATION_ID);

        if (releaseMediaPlayer && mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
        }
    }

    @Override
    public void onDestroy() {
        mState = STATE_STOPPED;
        relaxResources(true);

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return musicBinder;
    }
}
